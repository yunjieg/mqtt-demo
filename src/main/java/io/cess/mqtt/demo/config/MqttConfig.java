package io.cess.mqtt.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * @author wcl
 * @version 1.0
 * @date 2019/12/14 5:41 下午
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "mqtt.msg")
public class MqttConfig {
    /**
     * 可在阿里云控制台找到(实例id)
     */
    private String instanceId;
    /**
     * accessKey
     */
    private String accessKey;
    /**
     * 密钥
     */
    private String secretKey;
    /**
     * TCP 协议接入点
     */
    private String connectEndpoint;
    /**
     * 话题id
     */
    private String topicId;
    /**
     * 群组id
     */
    private String groupId;
    /**
     * 消息模式（广播订阅， 集群订阅）
     */
    private String messageModel;

    /**
     * 超时时间
     */
    private String sendMsgTimeoutMillis;

    /**
     * 顺序消息消费失败进行重试前的等待时间 单位(毫秒)
     */
    private String suspendTimeMillis;

    /**
     * 消息消费失败时的最大重试次数
     */
    private String maxReconsumeTimes;

    /**
     * 公网token服务器
     */
    private String mqttClientTokenServer;
    /**
     * 过期时间(默认1个月）
     */
    private Long mqttClientTokenExpireTime;
    /**
     * 分发给客户端的token的操作权限
     */
    private String mqttAction;
    /**
     * 客户端标识
     */
    private String clientId;

    /**
     * QoS参数代表传输质量，可选0，1，2，根据实际需求合理设置，具体参考 https://help.aliyun.com/document_detail/42420.html?spm=a2c4g.11186623.6.544.1ea529cfAO5zV3
     */
    private int qosLevel = 0;
    /**
     * 客户端超时时间
     */
    private  int timeToWait;
}
